package branchingPractical;

import java.util.Scanner;

public class DayType {

	public static void main(String[] args) {
		
		String Weekday = "";
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Enter the day:");
		int DayNumber = Integer.parseInt(myScanner.nextLine()); 
		
		if (DayNumber <=0 || DayNumber >7) {
		System.out.println("Please add Day Number from 1 - 7");
			return;}
		else if (DayNumber <6)
			System.out.println("It is a Working Day!");
		else
			System.out.println("It is a Holiday!");
		myScanner.close();
		//switch (DayNumber) {
//		case 1,2,3,4,5:
	///		System.out.println("It is a Working Day!");
	//		break;
		//case 6,7:
			//System.out.println("It is a Holiday!");
			//break;
		//// or switch (DayNumber) {
			///case 1:
			///case 2:
			///case 3: etc.
			///Weekday = "It is a Working Day";
			///break;

	}}