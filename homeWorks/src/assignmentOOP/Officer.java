package assignmentOOP;
public class Officer
{
    private String name;
    private String surname;
    private int officerID;
    private int crimesSolved;
    
    public Officer() {
    }
    
    public Officer(final String name, final String surname, final String workingDistrict, final int officerID, final int crimesSolved) {
        this.name = name;
        this.surname = surname;
        this.crimesSolved = crimesSolved;
        this.officerID = officerID;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getSurname() {
        return this.surname;
    }
    
    public void setSurname(final String surname) {
        this.surname = surname;
    }
    
    public int getOfficerID() {
        return this.officerID;
    }
    
    public void setOfficerID(final int officerID) {
        this.officerID = officerID;
    }
    
    public int getCrimesSolved() {
        return this.crimesSolved;
    }
    
    public void setCrimesSolved(final int crimesSolved) {
        this.crimesSolved = crimesSolved;
    }
    
    @Override
    public String toString() {
        return "Name: " + this.name + System.lineSeparator() + "Surname: " + this.surname + System.lineSeparator() + "Officer ID: " + this.officerID + System.lineSeparator() + System.lineSeparator() + "Crimes Solved: " + this.crimesSolved;
    }
    
    int calculateLevel() {
        if (this.crimesSolved < 20) {
            return 1;
        }
        if (this.crimesSolved >= 20 && this.crimesSolved < 40) {
            return 2;
        }
        return 3;
    }
}