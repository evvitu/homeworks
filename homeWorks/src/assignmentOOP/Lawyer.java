package assignmentOOP;

public class Lawyer extends Person
{
    private int lawyerID;
    private int helpedInCrimeSolving;
    public String name;
    public String surname;
    
    public void setLawyerID(final int lawyerID) {
        this.lawyerID = lawyerID;
    }
    
    public void sethHlpedInCrimeSolving(final int helpedInCrimeSolving) {
        this.helpedInCrimeSolving = helpedInCrimeSolving;
    }
    
    public int getLawyerIDt() {
        return this.lawyerID;
    }
    
    public int getHelpedInCrimeSolving() {
        return this.helpedInCrimeSolving;
    }
    
    public Lawyer(final String name, final String surname, final int lawyerID, final int helpedInCrimeSolving) {
        super(name, surname);
        this.lawyerID = lawyerID;
        this.helpedInCrimeSolving = helpedInCrimeSolving;
    }
    
    public static void main(final String[] args) {
        final Lawyer lawyer1 = new Lawyer("David", "Oak", 909, 56);
    }
    
    public String toString() {
        return "Name: " + this.name + System.lineSeparator() + "Surname: " + this.surname + "Lawyer ID: " + this.lawyerID + System.lineSeparator() + "Helped in crime solving: " + this.helpedInCrimeSolving;
    }
}