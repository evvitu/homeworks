package assignmentOOP;

import java.util.ArrayList;
import java.util.Collection;

public class Execution
{
    public static void main(final String[] args) {
        final Officer[] officers = new Officer[7];
        for (int i = 0; i < 7; ++i) {
            officers[i] = new Officer();
            
            
        officers[0].setCrimesSolved(21);
        officers[0].setName("Anna");
        officers[0].setSurname("Brown");
        officers[0].setOfficerID(55);
        
        officers[1].setCrimesSolved(10);
        officers[1].setName("Tom");
        officers[1].setSurname("Cruz");
        officers[1].setOfficerID(30);
        
        officers[2].setCrimesSolved(14);
        officers[2].setName("Tomy");
        officers[2].setSurname("Dowe");
        officers[2].setOfficerID(02);
        
        officers[3].setCrimesSolved(25);
        officers[3].setName("Amy");
        officers[3].setSurname("Jolly");
        officers[3].setOfficerID(36);
        
        officers[4].setCrimesSolved(14);
        officers[4].setName("Kevin");
        officers[4].setSurname("Malone");
        officers[4].setOfficerID(46);
        
        officers[5].setCrimesSolved(23);
        officers[5].setName("Steve");
        officers[5].setSurname("Schrute");
        officers[5].setOfficerID(66);
        
        officers[6].setCrimesSolved(9);
        officers[6].setName("Andrew");
        officers[6].setSurname("Mark");
        officers[6].setOfficerID(43);
       
        final District district1 = new District(" District1", "California", 234);
        final District district2 = new District(" District2", "NewYork", 245);
        for (int j = 0; j < 3; ++j) {
            district1.addNewOfficer(officers[j]);
        }
        for (int j = 3; j < 7; ++j) {
            district2.addNewOfficer(officers[j]);
        }
        System.out.println(district1);
        System.out.print(System.lineSeparator());
        System.out.println(district2);
        System.out.print(System.lineSeparator());
        district2.removerOfficer(officers[6]);
        System.out.println("Average level in the first district: " + district1.calculateAvgLevellInDistrict());
        System.out.println("Average level in the second district: " + district2.calculateAvgLevellInDistrict());
        System.out.println("There are " + district1.getOfficersInTheDistrict().size() + " officers in the first district");
        System.out.println("There are " + district2.getOfficersInTheDistrict().size() + " officers in the second district");
       
        final ArrayList<Officer> officersInTheFirstDistrict = district1.getOfficersInTheDistrict();
        final ArrayList<Officer> officersInTheSecondDistrict = district2.getOfficersInTheDistrict();
        final District commonDistrict = new District("District1", "London", 323);
        
        commonDistrict.getOfficersInTheDistrict().addAll(officersInTheFirstDistrict);
        commonDistrict.getOfficersInTheDistrict().addAll(officersInTheSecondDistrict);
        
        System.out.println(commonDistrict.calculateAvgLevellInDistrict());
        System.out.println("There are " + commonDistrict.getOfficersInTheDistrict().size() + " in Common district");
        if (district1.calculateAvgLevellInDistrict() == district2.calculateAvgLevellInDistrict()) {
            System.out.println("Levels are the same");
        }
        else {
            System.out.println((district1.calculateAvgLevellInDistrict() < district2.calculateAvgLevellInDistrict()) ? "District 2 has better av. level" : "District 1 has better level");
        }}}}
    
