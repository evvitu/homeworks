package assignmentOOP;

import java.util.ArrayList;
import java.util.Iterator;


public class District
{
    private String title;
    private String city;
    private int districtID;
    private ArrayList<Officer> officersInTheDistrict;
    
    public District(final String title, final String city, final int districtID) {
        this.officersInTheDistrict = new ArrayList<Officer>();
        this.city = city;
        this.districtID = districtID;
        this.title = title;
        this.city = city;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    public String getCity() {
        return this.city;
    }
    
    public void setCity(final String city) {
        this.city = city;
    }
    
    public int getDistrictID() {
        return this.districtID;
    }
    
    public void setDistrictID(final int districtID) {
        this.districtID = districtID;
    }
    
    @Override
    public String toString() {
        return "The title:" + this.title + System.lineSeparator() + "City: " + this.city + System.lineSeparator() + "District ID: " + this.districtID + System.lineSeparator() + "There are " + this.officersInTheDistrict + " in the district";
    }
    
    public void addNewOfficer(final Officer officer) {
        this.officersInTheDistrict.add(officer);
    }
    
    public void removerOfficer(final Officer officer) {
        this.officersInTheDistrict.remove(officer);
    }
    
    public float calculateAvgLevellInDistrict() {
        float levelSum = 0.0f;
   //     for (int i = 0; i < this.officersInTheDistrict.size(); ++i) {
    //       levelSum += this.officersInTheDistrict.get(i).calculateLevel();
    Iterator<Officer> iterator = this.officersInTheDistrict.iterator();
    while(iterator.hasNext()) {
    	levelSum += iterator.next().calculateLevel();
        	
        			
    }
        return levelSum;
    }
    
    public ArrayList<Officer> getOfficersInTheDistrict() {
        return this.officersInTheDistrict;
    }
    
    public void setOfficersInTheDistrict(final ArrayList<Officer> officersInTheDistrict) {
        this.officersInTheDistrict = officersInTheDistrict;
    }
}