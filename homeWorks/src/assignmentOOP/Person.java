package assignmentOOP;
public class Person
{
    private String name;
    private String surname;
    
    public Person() {
    }
    
    public Person(final String name, final String surname) {
        this.name = name;
        this.surname = surname;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getSurname() {
        return this.surname;
    }
    
    public void setSurname(final String surname) {
        this.surname = surname;
    }
    
    @Override
    public String toString() {
        return "Name: " + this.name + System.lineSeparator() + "Surname: " + this.surname + System.lineSeparator();
    }
}