package branchingPracitcal;

import java.util.Scanner;

public class MultipleCalculation {
	public static void main(String[] args) {

	Scanner myScanner = new Scanner(System.in);

	System.out.println("Enter the first number:");
	double number1 = Double.parseDouble(myScanner.nextLine());

	System.out.println("Enter operator ^,*, /, %, +, -, p, b, s");
	char operator = myScanner.nextLine().charAt(0);

	System.out.println("Enter the second number:");
	double number2 = myScanner.nextDouble();
	
	double outcome = 2;

	switch (operator) {
	case '*':
		outcome = number1 * number2;
		break;
	case '/':
		outcome = number1 / number2;
		break;
	case '+':
		outcome = number1 + number2;
		break;
	case '-':
		outcome = number1 - number2;
		break;
	case '%':
		outcome = number1 % number2;
		break;
	case 'p':
		System.out.format("%.1f '' %.1f", number1, number2); ////was not able to get it to work properly
		myScanner.close();
		
	case 'b':
		System.out.println("Biggest element:" + (number1 > number2 ? number1 : number2));	//// condition ? action when true : action when false; Math.max(number1,number2))
		myScanner.close();
		
	case 's':
		System.out.println("Smallest element:" + (Math.min(number1, number2)));	//// condition ? action when true : action when false; Math.max(number1,number2))
		myScanner.close();
		
	default:
		System.out.println("You've entered " + operator + ", which is incorrect");
		myScanner.close();
		return;}
	
	System.out.println("The result is " + outcome);
		myScanner.close();
}
}