package branchingPracitcal;

import java.util.Scanner;

public class StudentEvaluation {

	public static void main(String[] args) {
		
		char grade='A';

		System.out.println("Hey there Student! Enter your grade!:");
		Scanner myScanner = new Scanner(System.in);
		String evaluation = myScanner.nextLine();

		switch (grade) {
		case 'A':
			evaluation = ("Perfect! You are so clever!");
			break;
		case 'B', 'C':
			evaluation = ("Good! But You can do better!");
			break;
		case 'D', 'E':
			evaluation = ("It is not good! You should study!");
			break;
		case 'F':
			evaluation = ("Fail! You need to repeat the exam!");
		default:
			evaluation = "This grade is out of range. Please enter grade from A - F!";
			break;}
				
		System.out.println(evaluation);

	}

}
